﻿namespace Assynhron_cryptography
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabEncrypt = new System.Windows.Forms.TabPage();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabDecrypt = new System.Windows.Forms.TabPage();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabCreateKey = new System.Windows.Forms.TabPage();
            this.pasCRY = new System.Windows.Forms.TextBox();
            this.pasSGN = new System.Windows.Forms.TextBox();
            this.CreateCryptKey = new System.Windows.Forms.Button();
            this.CreateSingKey = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabEncrypt.SuspendLayout();
            this.tabDecrypt.SuspendLayout();
            this.tabCreateKey.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabEncrypt);
            this.tabControl1.Controls.Add(this.tabDecrypt);
            this.tabControl1.Controls.Add(this.tabCreateKey);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(335, 220);
            this.tabControl1.TabIndex = 1;
            // 
            // tabEncrypt
            // 
            this.tabEncrypt.Controls.Add(this.button10);
            this.tabEncrypt.Controls.Add(this.label7);
            this.tabEncrypt.Controls.Add(this.textBox7);
            this.tabEncrypt.Controls.Add(this.button5);
            this.tabEncrypt.Controls.Add(this.button4);
            this.tabEncrypt.Controls.Add(this.button3);
            this.tabEncrypt.Controls.Add(this.button1);
            this.tabEncrypt.Controls.Add(this.textBox3);
            this.tabEncrypt.Controls.Add(this.textBox2);
            this.tabEncrypt.Controls.Add(this.textBox1);
            this.tabEncrypt.Controls.Add(this.label3);
            this.tabEncrypt.Controls.Add(this.label2);
            this.tabEncrypt.Controls.Add(this.label1);
            this.tabEncrypt.Location = new System.Drawing.Point(4, 22);
            this.tabEncrypt.Name = "tabEncrypt";
            this.tabEncrypt.Padding = new System.Windows.Forms.Padding(3);
            this.tabEncrypt.Size = new System.Drawing.Size(327, 194);
            this.tabEncrypt.TabIndex = 1;
            this.tabEncrypt.Text = "Шифрование";
            this.tabEncrypt.UseVisualStyleBackColor = true;
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(183, 99);
            this.textBox7.Name = "textBox7";
            this.textBox7.PasswordChar = '*';
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 28;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(286, 135);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(24, 20);
            this.button5.TabIndex = 27;
            this.button5.Text = "...";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(286, 73);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(24, 20);
            this.button4.TabIndex = 26;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(286, 27);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 20);
            this.button3.TabIndex = 25;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(112, 161);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Зашифровать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(20, 135);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(263, 20);
            this.textBox3.TabIndex = 23;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(20, 73);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(263, 20);
            this.textBox2.TabIndex = 22;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(20, 27);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(263, 20);
            this.textBox1.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Открытый ключ шифрование";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Закрытый ключ цифровой подписи";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Файл";
            // 
            // tabDecrypt
            // 
            this.tabDecrypt.Controls.Add(this.button9);
            this.tabDecrypt.Controls.Add(this.label8);
            this.tabDecrypt.Controls.Add(this.textBox4);
            this.tabDecrypt.Controls.Add(this.button2);
            this.tabDecrypt.Controls.Add(this.button6);
            this.tabDecrypt.Controls.Add(this.button7);
            this.tabDecrypt.Controls.Add(this.button8);
            this.tabDecrypt.Controls.Add(this.textBox5);
            this.tabDecrypt.Controls.Add(this.textBox6);
            this.tabDecrypt.Controls.Add(this.textBox8);
            this.tabDecrypt.Controls.Add(this.label4);
            this.tabDecrypt.Controls.Add(this.label5);
            this.tabDecrypt.Controls.Add(this.label6);
            this.tabDecrypt.Location = new System.Drawing.Point(4, 22);
            this.tabDecrypt.Name = "tabDecrypt";
            this.tabDecrypt.Padding = new System.Windows.Forms.Padding(3);
            this.tabDecrypt.Size = new System.Drawing.Size(327, 194);
            this.tabDecrypt.TabIndex = 2;
            this.tabDecrypt.Text = "Дешифрование";
            this.tabDecrypt.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(183, 99);
            this.textBox4.Name = "textBox4";
            this.textBox4.PasswordChar = '*';
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 28;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(286, 135);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 20);
            this.button2.TabIndex = 27;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(286, 73);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(24, 20);
            this.button6.TabIndex = 26;
            this.button6.Text = "...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(286, 27);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(24, 20);
            this.button7.TabIndex = 25;
            this.button7.Text = "...";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(112, 161);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(90, 23);
            this.button8.TabIndex = 24;
            this.button8.Text = "Дешифровать";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(20, 135);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(263, 20);
            this.textBox5.TabIndex = 23;
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(20, 73);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(263, 20);
            this.textBox6.TabIndex = 22;
            this.textBox6.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(20, 27);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(263, 20);
            this.textBox8.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Открытый цифровой подписи";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Закрытый ключ шифрования";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Файл";
            // 
            // tabCreateKey
            // 
            this.tabCreateKey.Controls.Add(this.button12);
            this.tabCreateKey.Controls.Add(this.button11);
            this.tabCreateKey.Controls.Add(this.label10);
            this.tabCreateKey.Controls.Add(this.label9);
            this.tabCreateKey.Controls.Add(this.pasCRY);
            this.tabCreateKey.Controls.Add(this.pasSGN);
            this.tabCreateKey.Controls.Add(this.CreateCryptKey);
            this.tabCreateKey.Controls.Add(this.CreateSingKey);
            this.tabCreateKey.Location = new System.Drawing.Point(4, 22);
            this.tabCreateKey.Name = "tabCreateKey";
            this.tabCreateKey.Size = new System.Drawing.Size(327, 194);
            this.tabCreateKey.TabIndex = 3;
            this.tabCreateKey.Text = "Создать ключи";
            this.tabCreateKey.UseVisualStyleBackColor = true;
            // 
            // pasCRY
            // 
            this.pasCRY.Location = new System.Drawing.Point(79, 112);
            this.pasCRY.Name = "pasCRY";
            this.pasCRY.PasswordChar = '*';
            this.pasCRY.Size = new System.Drawing.Size(163, 20);
            this.pasCRY.TabIndex = 3;
            // 
            // pasSGN
            // 
            this.pasSGN.Location = new System.Drawing.Point(79, 34);
            this.pasSGN.Name = "pasSGN";
            this.pasSGN.PasswordChar = '*';
            this.pasSGN.Size = new System.Drawing.Size(163, 20);
            this.pasSGN.TabIndex = 2;
            // 
            // CreateCryptKey
            // 
            this.CreateCryptKey.Location = new System.Drawing.Point(3, 138);
            this.CreateCryptKey.Name = "CreateCryptKey";
            this.CreateCryptKey.Size = new System.Drawing.Size(315, 23);
            this.CreateCryptKey.TabIndex = 1;
            this.CreateCryptKey.Text = "Создание ключей шифрования";
            this.CreateCryptKey.UseVisualStyleBackColor = true;
            this.CreateCryptKey.Click += new System.EventHandler(this.CreateCryptKey_Click);
            // 
            // CreateSingKey
            // 
            this.CreateSingKey.Location = new System.Drawing.Point(3, 60);
            this.CreateSingKey.Name = "CreateSingKey";
            this.CreateSingKey.Size = new System.Drawing.Size(315, 23);
            this.CreateSingKey.TabIndex = 0;
            this.CreateSingKey.Text = "Создание ключей цифровой подписи";
            this.CreateSingKey.UseVisualStyleBackColor = true;
            this.CreateSingKey.Click += new System.EventHandler(this.CreateSingKey_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(77, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Пароль для ключа";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(77, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "Пароль для ключа";
            // 
            // button9
            // 
            this.button9.Enabled = false;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button9.Location = new System.Drawing.Point(286, 99);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(24, 20);
            this.button9.TabIndex = 31;
            this.button9.Text = "*";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button9_MouseDown);
            this.button9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button9_MouseUp);
            // 
            // button10
            // 
            this.button10.Enabled = false;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button10.Location = new System.Drawing.Point(286, 99);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(24, 20);
            this.button10.TabIndex = 32;
            this.button10.Text = "*";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button10_MouseDown);
            this.button10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button10_MouseUp);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(76, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Пароль для ключей";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(76, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Пароль для ключей";
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button11.Location = new System.Drawing.Point(248, 112);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(24, 20);
            this.button11.TabIndex = 32;
            this.button11.Text = "*";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button11_MouseDown);
            this.button11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button11_MouseUp);
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button12.Location = new System.Drawing.Point(248, 34);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(24, 20);
            this.button12.TabIndex = 33;
            this.button12.Text = "*";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button12_MouseDown);
            this.button12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button12_MouseUp);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 221);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RSA";
            this.tabControl1.ResumeLayout(false);
            this.tabEncrypt.ResumeLayout(false);
            this.tabEncrypt.PerformLayout();
            this.tabDecrypt.ResumeLayout(false);
            this.tabDecrypt.PerformLayout();
            this.tabCreateKey.ResumeLayout(false);
            this.tabCreateKey.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabEncrypt;
        private System.Windows.Forms.TabPage tabDecrypt;
        private System.Windows.Forms.TabPage tabCreateKey;
        private System.Windows.Forms.Button CreateCryptKey;
        private System.Windows.Forms.Button CreateSingKey;
        private System.Windows.Forms.TextBox pasCRY;
        private System.Windows.Forms.TextBox pasSGN;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;

    }
}

