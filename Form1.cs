﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Assynhron_cryptography
{
    public partial class Form1 : Form
    {
        private const int KeyLen = 1024;
        RSAParameters rsasgnpriv, rsasgnpub;
        RSAParameters rsaencpub, rsaencpriv;
        SaveFileDialog sfd;
        OpenFileDialog ofd;
        private const int MaxRSABlockSize = KeyLen / 8 - 42;
        public Form1()
        {
            InitializeComponent();
            sfd = new SaveFileDialog();
            ofd = new OpenFileDialog();
            ofd.Multiselect = false;
        }

        private void CreateSingKey_Click(object sender, EventArgs e)
        {
            if(pasSGN.Text=="")
            {
                MessageBox.Show("Введите пароль ключа");
                return;
            }
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(KeyLen);
            rsasgnpriv = rsa.ExportParameters(true);
            rsasgnpub = rsa.ExportParameters(false);
            sfd.DefaultExt = ".sgnpriv";
            sfd.Title = "Выберите файл для сохранения приватного ключа подписи";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                    SerializeRSAKey(rsasgnpriv, sfd.FileName, pasSGN.Text);
            }
            sfd.FileName = Path.GetFileNameWithoutExtension(sfd.FileName);
            sfd.DefaultExt = ".sgnpub";
            sfd.Title = "Выберите файл для сохранения публичного ключа подписи";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                SerializeRSAKey(rsasgnpub, sfd.FileName, null);
            }
            MessageBox.Show("Сгенерирована новая ключевая пара для подписи RSA");
        }

        private void CreateCryptKey_Click(object sender, EventArgs e)
        {
            if (pasCRY.Text == "")
            {
                MessageBox.Show("Введите пароль ключа");
                return;
            }
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(KeyLen);
            rsaencpriv = rsa.ExportParameters(true);
            rsaencpub = rsa.ExportParameters(false);
            sfd.DefaultExt = ".encpriv";
            sfd.Title = "Выберите файл для сохранения приватного ключа шифрования";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                    SerializeRSAKey(rsaencpriv, sfd.FileName, pasCRY.Text);
            }
            sfd.FileName = Path.GetFileNameWithoutExtension(sfd.FileName);
            sfd.DefaultExt = ".encpub";
            sfd.Title = "Выберите файл для сохранения публичного ключа шифрования";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                SerializeRSAKey(rsaencpub, sfd.FileName, null);
            }
            MessageBox.Show("Сгенерирована новая ключевая пара для шифрования");
        }

        private void SerializeRSAKey(RSAParameters rp, string file, string pass)
        {
            FileStream fs;
            using (fs = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, rp.Exponent);
                bf.Serialize(fs, rp.Modulus);
                if (pass != null)
                {
                    MemoryStream ms = new MemoryStream();
                    bf.Serialize(ms, rp.P);
                    bf.Serialize(ms, rp.Q);
                    bf.Serialize(ms, rp.D);
                    bf.Serialize(ms, rp.InverseQ);
                    bf.Serialize(ms, rp.DP);
                    bf.Serialize(ms, rp.DQ);
                    PasswordDeriveBytes pdb = new PasswordDeriveBytes(pass, null);
                    pdb.HashName = "SHA256";
                    Rijndael rjn = (Rijndael)new RijndaelManaged();
                    rjn.KeySize = 256;
                    rjn.Key = pdb.GetBytes(256 / 8);
                    rjn.IV = new byte[rjn.BlockSize / 8];
                    ICryptoTransform tr = rjn.CreateEncryptor();
                    byte[] arr = ms.ToArray();
                    arr = tr.TransformFinalBlock(arr, 0, arr.Length);
                    fs.Write(arr, 0, arr.Length);
                    rjn.Clear();
                    ms.Close();
                }
                fs.Close();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            ofd.Filter = "";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = ofd.FileName;
                ofd.FileName = "";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Private crypt-key|*.sgnpriv" + "|All Files|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = ofd.FileName;
                ofd.FileName = "";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Open crypto-key|*.encpub" + "|All Files|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox3.Text = ofd.FileName;
                ofd.FileName = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox7.Text == "")
            {
                MessageBox.Show("Заполните все поля для данных, прежде чем начать.");
                return;
            }
            sfd.Title = "Выберите файл для сохранения результата";
            sfd.DefaultExt = ".enc";
            try
            {
                rsaencpub = DeserializeRSAKey(textBox3.Text, null);
                rsasgnpriv = DeserializeRSAKey(textBox2.Text, textBox7.Text);
            }
            catch(Exception)
            {
                MessageBox.Show("Ошибка загрузки ключа");
            }
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                FileStream fin, fout;
                using(fin = new FileStream(textBox1.Text,FileMode.Open,FileAccess.Read,FileShare.Read))
                {
                    fout = new FileStream(sfd.FileName, FileMode.Create, FileAccess.Write, FileShare.Read);
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(fout, "RSA"); 
                    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(KeyLen);
                    RSAParameters tmp = new RSAParameters(); 
                    tmp.Exponent = (byte[])rsaencpub.Exponent.Clone();
                    tmp.Modulus = (byte[])rsaencpub.Modulus.Clone();
                    rsa.ImportParameters(tmp); 

                    byte[] inbuf = new byte[MaxRSABlockSize];
                    byte[] outbuf = new byte[MaxRSABlockSize];
                    SHA1CryptoServiceProvider ha = new SHA1CryptoServiceProvider();
                    bf.Serialize(fout, "SHA1");
                    ha.Initialize();

                    int len;
					while((len=fin.Read(inbuf,0,MaxRSABlockSize))==MaxRSABlockSize)
					{
						ha.TransformBlock(inbuf,0,MaxRSABlockSize,outbuf,0); 
						byte []enc=rsa.Encrypt(inbuf,true); 
						fout.Write(enc,0,enc.Length);
					}
					inbuf=ha.TransformFinalBlock(inbuf,0,len); 
					outbuf=rsa.Encrypt(inbuf,true); 
					fout.Write(outbuf,0,outbuf.Length);
					AsymmetricSignatureFormatter sf;
					tmp.D=(byte [])rsasgnpriv.D.Clone();
					tmp.DP=(byte [])rsasgnpriv.DP.Clone();
					tmp.DQ=(byte [])rsasgnpriv.DQ.Clone();
					tmp.Exponent=(byte [])rsasgnpriv.Exponent.Clone();
					tmp.InverseQ=(byte [])rsasgnpriv.InverseQ.Clone();
					tmp.Modulus=(byte [])rsasgnpriv.Modulus.Clone();
					tmp.P=(byte [])rsasgnpriv.P.Clone();
					tmp.Q=(byte [])rsasgnpriv.Q.Clone();
					rsa.ImportParameters(tmp);
					sf=(AsymmetricSignatureFormatter)new RSAPKCS1SignatureFormatter(rsa);
					sf.SetHashAlgorithm("SHA1");
					outbuf=sf.CreateSignature(ha.Hash);
					fout.Write(outbuf,0,outbuf.Length);
					fout.Close();
					fin.Close();
                }

            }
            MessageBox.Show("Файл успешно зашифрован");
        }

        private RSAParameters DeserializeRSAKey(string file, string pass)
        {
            FileStream fs;
            RSAParameters rp = new RSAParameters();
            using (fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                BinaryFormatter bf = new BinaryFormatter();
                rp.Exponent = (byte[])bf.Deserialize(fs); 
                rp.Modulus = (byte[])bf.Deserialize(fs);
                if (pass != null) 
                {
                    long len = fs.Length - fs.Position;
                    byte[] arr = new byte[len];
                    len = fs.Read(arr, 0, (int)len);
                    PasswordDeriveBytes pdb = new PasswordDeriveBytes(pass, null); //генерируем ключ экспорта
                    pdb.HashName = "SHA256"; 
                    Rijndael rjn = (Rijndael)new RijndaelManaged();
                    rjn.KeySize = 256;
                    rjn.Key = pdb.GetBytes(256 / 8);
                    rjn.IV = new byte[rjn.BlockSize / 8];
                    ICryptoTransform tr = rjn.CreateDecryptor();
                    arr = tr.TransformFinalBlock(arr, 0, (int)len);
                    rjn.Clear();
                    MemoryStream ms = new MemoryStream(arr, false);
                    rp.P = (byte[])bf.Deserialize(ms);
                    rp.Q = (byte[])bf.Deserialize(ms);
                    rp.D = (byte[])bf.Deserialize(ms);
                    rp.InverseQ = (byte[])bf.Deserialize(ms);
                    rp.DP = (byte[])bf.Deserialize(ms);
                    rp.DQ = (byte[])bf.Deserialize(ms);
                    ms.Close();
                }
                fs.Close();
                return rp;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if(textBox2.Text!="")
            {
                textBox7.Enabled = true;
                button10.Enabled = true;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ofd.Filter = "";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox8.Text = ofd.FileName;
                ofd.FileName = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Open crypt-key|*.sgnpub" + "|All Files|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox5.Text = ofd.FileName;
                ofd.FileName = "";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ofd.Filter = "Private key sign|*.encpriv" + "|All Files|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox6.Text = ofd.FileName;
                ofd.FileName = "";
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (textBox6.Text != "")
            {
                textBox4.Enabled = true;
                button9.Enabled = true;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (textBox8.Text == "" || textBox6.Text == "" || textBox5.Text == "" || textBox4.Text=="")
            {
                MessageBox.Show("Заполните все поля для данных, прежде чем начать.");
                return;
            }
            sfd.Title = "Выберите файл для сохранения результата";
            sfd.DefaultExt = ".dec";
            try
            {
                rsasgnpub = DeserializeRSAKey(textBox5.Text, null);
                rsaencpriv = DeserializeRSAKey(textBox6.Text, textBox4.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка загрузки ключа");
            }
            if(sfd.ShowDialog()==DialogResult.OK)
            {
                FileStream fin,fout;
                using (fin = new FileStream(textBox8.Text, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    fout = new FileStream(sfd.FileName, FileMode.Create, FileAccess.Write, FileShare.Read);
                    BinaryFormatter bf=new BinaryFormatter();
					string checksingn=(string)bf.Deserialize(fin); 
                    if (checksingn == "RSA")
                    {
                        string hname = (string)bf.Deserialize(fin);
                        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(KeyLen);
                        RSAParameters tmp = new RSAParameters();
                        tmp.D = (byte[])rsaencpriv.D.Clone();
                        tmp.DP = (byte[])rsaencpriv.DP.Clone();
                        tmp.DQ = (byte[])rsaencpriv.DQ.Clone();
                        tmp.Exponent = (byte[])rsaencpriv.Exponent.Clone();
                        tmp.InverseQ = (byte[])rsaencpriv.InverseQ.Clone();
                        tmp.Modulus = (byte[])rsaencpriv.Modulus.Clone();
                        tmp.P = (byte[])rsaencpriv.P.Clone();
                        tmp.Q = (byte[])rsaencpriv.Q.Clone();
                        rsa.ImportParameters(tmp); 
                        byte[] inbuf = new byte[KeyLen / 8];
                        byte[] outbuf = new byte[MaxRSABlockSize];
                        SHA1CryptoServiceProvider ha = new SHA1CryptoServiceProvider();
                        ha.Initialize();
                        try
                        {
                            while (fin.Position < fin.Length - KeyLen / 8)
                            {
                                fin.Read(inbuf, 0, KeyLen / 8);
                                byte[] dec = rsa.Decrypt(inbuf, true); 
                                ha.TransformBlock(dec, 0, dec.Length, outbuf, 0);
                                fout.Write(dec, 0, dec.Length);
                            }
                            outbuf = new byte[0];
                            outbuf = ha.TransformFinalBlock(outbuf, 0, 0);
                            AsymmetricSignatureDeformatter df;
                            byte[] sig;
                            tmp = new RSAParameters();
                            tmp.Exponent = (byte[])rsasgnpub.Exponent.Clone();
                            tmp.Modulus = (byte[])rsasgnpub.Modulus.Clone();
                            rsa.ImportParameters(tmp);
                            df = (AsymmetricSignatureDeformatter)new RSAPKCS1SignatureDeformatter(rsa);
                            sig = inbuf;
                            fin.Read(sig, 0, sig.Length); 
                            df.SetHashAlgorithm(hname);
                            if (df.VerifySignature(ha.Hash, sig)) MessageBox.Show("Файл успешно дешифрован");
                            else MessageBox.Show("Файл дешифрован, но подпись не верна");
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка дешифровки файла.");
                        }
                        fout.Close();
                        fin.Close();
                    }
                    else MessageBox.Show("Алгоритм подписи " + checksingn + "не поддерживается");

                }
            }
        }

        private void button9_MouseDown(object sender, MouseEventArgs e)
        {
            textBox4.PasswordChar = (char)0;
        }

        private void button9_MouseUp(object sender, MouseEventArgs e)
        {
            textBox4.PasswordChar = '*';
        }

        private void button10_MouseDown(object sender, MouseEventArgs e)
        {
            textBox7.PasswordChar = (char)0;
        }

        private void button10_MouseUp(object sender, MouseEventArgs e)
        {
            textBox7.PasswordChar = '*';
        }

        private void button11_MouseUp(object sender, MouseEventArgs e)
        {
            pasCRY.PasswordChar = '*';
        }

        private void button11_MouseDown(object sender, MouseEventArgs e)
        {
            pasCRY.PasswordChar = (char)0;
        }

        private void button12_MouseDown(object sender, MouseEventArgs e)
        {
            pasSGN.PasswordChar = (char)0;
        }

        private void button12_MouseUp(object sender, MouseEventArgs e)
        {
            pasSGN.PasswordChar = '*';
        }
    }
}
